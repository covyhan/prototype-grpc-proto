# API Reference

# Table of Contents


- Services
    - [Talk](#talktalk)
  





# <div id="talktalk">Talk</div>


## sendTalk

> **rpc** sendTalk([SendTalkRequest](#sendtalkrequest))
    [SendTalkResponse](#sendtalkresponse)


 <!-- end methods -->
 <!-- end services -->

# Messages


## <div id="sendtalkrequest">SendTalkRequest</div>



| Field | Type | Description |
| ----- | ---- | ----------- |
| token | [ string](#string) |  |
| target_uid | [ string](#string) |  |
| push_uids | [repeated string](#string) |  |
| content | [ string](#string) |  |
| extra | [ string](#string) |  |
| message_type | [ int32](#int32) |  |
| visibility | [ SendTalkRequest.MessageVisibility](#sendtalkrequestmessagevisibility) |  |
 <!-- end Fields -->
 <!-- end HasFields -->


## <div id="sendtalkresponse">SendTalkResponse</div>



| Field | Type | Description |
| ----- | ---- | ----------- |
| result | [ string](#string) |  |
| message_id | [ string](#string) |  |
 <!-- end Fields -->
 <!-- end HasFields -->
 <!-- end messages -->

# Enums


## <div id="sendtalkrequestmessagevisibility">SendTalkRequest.MessageVisibility</div>


| Name | Number | Description |
| ---- | ------ | ----------- |
| NONE | 0 |  |
| ALL | 1 |  |


 <!-- end Enums -->
 <!-- end Files -->