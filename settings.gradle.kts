rootProject.name = "prototype-grpc-proto"

dependencyResolutionManagement {
    versionCatalogs {
        create("libs") {
            version("grpc", "1.39.0")
            version("protobuf", "3.19.2")
            version("grpckt", "1.2.1")
            version("pgv", "0.6.3")

            library("kotlin-coroutines", "org.jetbrains.kotlinx", "kotlinx-coroutines-core").version("1.5.2")
            library("grpc-protobuf", "io.grpc", "grpc-protobuf").versionRef("grpc")
            library("protobuf-java", "com.google.protobuf", "protobuf-java-util").versionRef("protobuf")
            library("protobuf-kotlin", "com.google.protobuf", "protobuf-kotlin").versionRef("protobuf")
            library("grpc-kotlin-stub", "io.grpc", "grpc-kotlin-stub").version("1.2.1")
            library("pgv-java", "io.envoyproxy.protoc-gen-validate", "pgv-java-stub").versionRef("pgv")

            bundle(
                "shared",
                listOf("kotlin-coroutines", "grpc-protobuf", "protobuf-java", "protobuf-kotlin", "grpc-kotlin-stub", "pgv-java"))
        }
    }
}

include("talk", "delivery")