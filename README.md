## 프로젝트 구조
- multiproject 구조이고, 각 subproject들은 하나의 domain api에서 제공하는 grpc interface를 가지고 있음.
- 각 subproject들은 독립적인 배포주기를 가지고 있어서 CI/CD system은 변경된 subproject만 새로 build해서 maven repository에 publish해야함

## subproject build 하기
환경별로 stub library가 관리되어야하므로, production 전용 코드는 master branch, 
staging 전용 코드는 staging branch를 사용한다고 가정했을 때, 
CI/CD system에서는 각 브랜치의 이름을 보고 production library를 build해야할 지,
staging library를 build 해야할 지 결정하면 됨

예를 들어서,

master branch > talk의 interface 변경 > 아래의 명령어 실행
> ./gradlew talk:publish -Penv=production

local maven repository에 publish하고 싶으면,
> ./gradlew talk:publishToMavenLocal -Penv=production
