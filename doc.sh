#!/bin/sh

if [[ $# -ne 1 ]];
then
  echo "need only one argument - subproject name"
  exit 1
fi

TARGET_SUBPROJECT=$1

docker run --rm \
  -v $(pwd)/${TARGET_SUBPROJECT}/doc:/out \
  -v $(pwd)/${TARGET_SUBPROJECT}/src/main/proto:/protos \
  -v $(pwd)/template:/template \
  pseudomuto/protoc-gen-doc --doc_opt=template/md.tmpl,docs.md

