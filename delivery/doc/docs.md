# API Reference

# Table of Contents


- Services
    - [Delivery](#deliverydelivery)
  





# <div id="deliverydelivery">Delivery</div>


## getPurchaseInfo

> **rpc** getPurchaseInfo([GetPurchaseInfoRequest](#getpurchaseinforequest))
    [GetPurchaseInfoResponse](#getpurchaseinforesponse)


 <!-- end methods -->
 <!-- end services -->

# Messages


## <div id="getpurchaseinforequest">GetPurchaseInfoRequest</div>



| Field | Type | Description |
| ----- | ---- | ----------- |
| purchase_id | [ int64](#int64) |  |
 <!-- end Fields -->
 <!-- end HasFields -->


## <div id="getpurchaseinforesponse">GetPurchaseInfoResponse</div>



| Field | Type | Description |
| ----- | ---- | ----------- |
| purchase_id | [ int64](#int64) |  |
| cvs_type | [ int32](#int32) |  |
| delivery_status | [ GetPurchaseInfoResponse.DeliveryStatus](#getpurchaseinforesponsedeliverystatus) |  |
| order_id | [ int64](#int64) |  |
| delivery_track_num | [ string](#string) |  |
 <!-- end Fields -->
 <!-- end HasFields -->
 <!-- end messages -->

# Enums


## <div id="getpurchaseinforesponsedeliverystatus">GetPurchaseInfoResponse.DeliveryStatus</div>


| Name | Number | Description |
| ---- | ------ | ----------- |
| NONE | 0 |  |
| IN_PROGRESS | 1 |  |
| DONE | 2 |  |


 <!-- end Enums -->
 <!-- end Files -->