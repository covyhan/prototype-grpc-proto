plugins {
    kotlin("jvm") version "1.6.10" apply false
    id("com.google.protobuf") version "0.8.18" apply false
    id("idea")
}

ext {
    set("env", if (project.hasProperty("env")) project.property("env") else "staging")
}

allprojects {
    group = "com.example.proto"

    repositories {
        mavenCentral()
        google()
    }
}

idea {
    module {
        sourceDirs.plus(file("src/generated/main/java"))
        sourceDirs.plus(file("src/generated/main/grpc"))
        generatedSourceDirs.plus(file("src/generated/main/java"))
        generatedSourceDirs.plus(file("src/generated/main/grpc"))
    }
}